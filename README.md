## Compilation

Compilation requires an OpenCL installable client driver (ICD) loader library to be installed as
well as the OpenCL header files. When running the program, there needs to be at least one OpenCL
platform installed.

```sh
# Set up the build directory in `build_directory_name`
meson setup <build_directory_name>

# Compile the project
meson compile -C <build_directory_name>
```

> This is just for learning and code quality is garbage.

## Formatting

```sh
clang-format -i {src/vendor}/**/*.{c,h} src/**/*.cl
```

## Note on Memory Safety

There is none. I'm a Rust soydev. I can't be bothered to clean up my own memory atrocities. And
besides, this isn't production code. The kernel will clean up anyway.
