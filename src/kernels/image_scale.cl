__kernel void image_scale_2x(__global float* image_input,
    int image_width,
    __global float* image_output,
    int image_output_width)
{
    int col = get_global_id(0);
    int row = get_global_id(1);

    image_output[row * image_output_width + col]
        = image_input[row / 2 * image_width + col / 2];
}
