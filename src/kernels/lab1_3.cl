__kernel void copy_text(__global char* input_text, __global char* output_text)
{
    int tid = get_global_id(0);

    output_text[tid] = input_text[tid];
}
