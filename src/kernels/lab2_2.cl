__kernel void matrix_multiply(__global int* input_matrix1,
    __global int* input_matrix2,
    __global int* output_matrix,
    int input_matrix1_rows,
    int input_matrix_equal_size,
    int input_matrix2_cols)
{
    int row = get_global_id(0);
    int col = get_global_id(1);
    int result = 0;

    for (int i = 0; i < input_matrix_equal_size; i++) {
        result += input_matrix1[i * input_matrix1_rows + row]
            * input_matrix2[col * input_matrix_equal_size + i];
    }

    output_matrix[col * input_matrix1_rows + row] = result;
}
