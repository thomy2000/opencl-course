__kernel void multiply_vectors(
    __global int* vector1, __global int* vector2, __global int* result_vector)
{
    int vector_index = get_global_id(0);

    result_vector[vector_index] = vector1[vector_index] * vector2[vector_index];
}
