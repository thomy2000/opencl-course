__kernel void nbodies(__global float* bodies, int bodies_amount)
{
    int index = get_global_id(0);

    float fx = 0.0f;
    float fy = 0.0f;
    float fz = 0.0f;

    for (int j = 0; j < bodies_amount; j++) {
        float dx = bodies[j * 6 + 0] - bodies[index * 6 + 0];
        float dy = bodies[j * 6 + 1] - bodies[index * 6 + 1];
        float dz = bodies[j * 6 + 2] - bodies[index * 6 + 2];
        float distance_squared = dx * dx + dy * dy + dz * dz + 0.000000001;
        float inverse_distance = 1.0f / sqrt(distance_squared);
        float inverse_distance3
            = inverse_distance * inverse_distance * inverse_distance;

        fx = dx * inverse_distance3;
        fy = dy * inverse_distance3;
        fz = dz * inverse_distance3;
    }

    bodies[index * 6 + 3] += 0.01f * fx;
    bodies[index * 6 + 4] += 0.01f * fy;
    bodies[index * 6 + 5] += 0.01f * fz;
}
