__kernel void gaussian_blur_three(__global float* input_image_data,
    int input_image_size,
    __global float* output_image_data)
{
    float gaussian_kernel[] = { 1.0 / 16,
        2.0 / 16,
        1.0 / 16,
        2.0 / 16,
        4.0 / 16,
        2.0 / 16,
        1.0 / 16,
        2.0 / 16,
        1.0 / 16 };
    float pixel_value = 0;
    int col = get_global_id(0);
    int row = get_global_id(1);

    for (int i = -1; i < 2; i++) {
        for (int j = -1; j < 2; j++) {
            float current_pixel
                = input_image_data[col + j + (row + i) * input_image_size];
            pixel_value += current_pixel * gaussian_kernel[j + 1 + (i + 1) * 3];
        }
    }

    output_image_data[col + row * input_image_size] = pixel_value;
}
