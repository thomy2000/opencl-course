__kernel void build_array(__global int* number_array)
{
    int tid = get_global_id(0);

    number_array[tid] = tid;
}
