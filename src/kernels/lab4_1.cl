__kernel void matrix_multiply(__global int* input_matrix1,
    __global int* input_matrix2,
    __global int* output_matrix,
    int input_matrix1_rows,
    int input_matrix_equal_size,
    int input_matrix2_cols)
{
    int result = 0;
    int col = get_global_id(0);
    int row = get_global_id(1);

    for (int i = 0; i < input_matrix_equal_size; i++) {
        result += input_matrix1[row * input_matrix_equal_size + i]
            * input_matrix2[i * input_matrix2_cols + col];
    }

    output_matrix[row * input_matrix2_cols + col] = result;
}
