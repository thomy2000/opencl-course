__kernel void matrix_transpose(__global int* matrix_input,
    int matrix_input_rows,
    int matrix_input_cols,
    __global int* matrix_output)
{
    int row = get_global_id(0);
    int col = get_global_id(1);

    matrix_output[row * matrix_input_cols + col]
        = matrix_input[row * matrix_input_cols + col];
}
