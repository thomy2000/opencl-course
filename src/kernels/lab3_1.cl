__kernel void edge_detection_sobel(__global float* input_image_data,
    int input_image_size,
    __global float* output_image_data)
{
    float x_kernel[] = { 1, 0, -1, 2, 0, -2, 1, 0, -1 };
    float y_kernel[] = { 1, 2, 1, 0, 0, 0, -1, -2, -1 };
    float gx = 0;
    float gy = 0;
    int col = get_global_id(0);
    int row = get_global_id(1);

    for (int i = -1; i < 2; i++) {
        for (int j = -1; j < 2; j++) {
            float current_pixel
                = input_image_data[col + j + (row + i) * input_image_size];
            gx += current_pixel * x_kernel[j + 1 + (i + 1) * 3];
            gy += current_pixel * y_kernel[j + 1 + (i + 1) * 3];
        }
    }

    output_image_data[col + row * input_image_size] = sqrt(gx * gx + gy * gy);
}
