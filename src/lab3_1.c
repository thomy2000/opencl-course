#include "opencl-setup.h"

#include <CL/cl.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmpfuncs.h"

typedef struct image {
    float* data;
    int width;
    int height;
} image;

int main(void)
{
    size_t global_work_size[2];
    size_t global_work_offset[] = { 1, 1 };

    opencl_t* opencl;
    image input_image;
    image output_image;
    cl_mem opencl_input_image_buffer;
    cl_mem opencl_output_image_buffer;
    cl_kernel blur_kernel;
    cl_kernel edge_detection_kernel;

    input_image.data = readImage("/tmp/temporary_in.bmp",
        &input_image.width,
        &input_image.height);
    output_image.width = input_image.width;
    output_image.height = input_image.height;
    output_image.data
        = malloc(sizeof(float) * output_image.width * output_image.height);
    global_work_size[0] = input_image.width - 2;
    global_work_size[1] = input_image.height - 2;

    // Create OpenCL objects
    opencl = cl_create_context_cli();
    opencl_input_image_buffer = cl_create_buffer(opencl,
        CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
        input_image.width * input_image.height * sizeof(float),
        (void*)input_image.data);
    opencl_output_image_buffer = cl_create_buffer(opencl,
        CL_MEM_READ_WRITE,
        output_image.width * output_image.height * sizeof(float),
        NULL);

    // Create kernel and add arguments
    blur_kernel = cl_create_kernel_with_source(opencl,
        "src/kernels/gaussian_blur_three.cl",
        "gaussian_blur_three");
    cl_set_kernel_arg(blur_kernel,
        0,
        sizeof(size_t),
        (void*)&opencl_input_image_buffer);
    cl_set_kernel_arg(blur_kernel,
        1,
        sizeof(cl_int),
        (void*)&input_image.width);
    cl_set_kernel_arg(blur_kernel,
        2,
        sizeof(size_t),
        (void*)&opencl_output_image_buffer);

    // Enqueue the kernel for execution and wait for it to finish
    cl_enqueue_nd_range_kernel(opencl,
        blur_kernel,
        2,
        global_work_offset,
        global_work_size,
        NULL,
        0,
        NULL,
        NULL);
    cl_finish(opencl);

    // Create kernel and add arguments
    edge_detection_kernel = cl_create_kernel_with_source(opencl,
        "src/kernels/lab3_1.cl",
        "edge_detection_sobel");
    cl_set_kernel_arg(edge_detection_kernel,
        0,
        sizeof(size_t),
        (void*)&opencl_output_image_buffer);
    cl_set_kernel_arg(edge_detection_kernel,
        1,
        sizeof(cl_int),
        (void*)&input_image.width);
    cl_set_kernel_arg(edge_detection_kernel,
        2,
        sizeof(size_t),
        (void*)&opencl_output_image_buffer);

    // Enqueue the kernel for execution and wait for it to finish
    cl_enqueue_nd_range_kernel(opencl,
        edge_detection_kernel,
        2,
        global_work_offset,
        global_work_size,
        NULL,
        0,
        NULL,
        NULL);
    cl_finish(opencl);

    // Read the data from the buffer back to the host
    cl_enqueue_read_buffer(opencl,
        opencl_output_image_buffer,
        CL_TRUE,
        0,
        output_image.width * output_image.height * sizeof(float),
        (void*)output_image.data,
        0,
        NULL,
        NULL);

    storeImage(output_image.data,
        "/tmp/temporary_out.bmp",
        output_image.height,
        output_image.width,
        "/tmp/temporary_in.bmp");

    // Free the memory we are responsible for managing!
    cl_destroy_wrapper(opencl);
    cl_release_kernel(blur_kernel);
    cl_release_kernel(edge_detection_kernel);
    cl_release_memory_object(opencl_input_image_buffer);
    cl_release_memory_object(opencl_output_image_buffer);
    // BUG: This doesn't actualy free any device, only the memory allocation
    // that holds them!
    free(input_image.data);
    free(output_image.data);

    return 0;
}
