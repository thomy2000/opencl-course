/*
 * Fill `matrix` with random numbers.
 */
void fill_matrix_random(int rows, int cols, int matrix[rows][cols]);

/*
 * Pretty print the given matrix.
 */
void print_matrix(int rows, int cols, int matrix[rows][cols]);
