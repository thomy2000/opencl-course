// A small program to use with Valgrind to check whether my code contains memory
// leaks, or the OpenCL library is causing them.
#ifndef CL_TARGET_OPENCL_VERSION
#define CL_TARGET_OPENCL_VERSION 200
#endif

#include "opencl-setup.h"
#include <stdio.h>

#include <CL/cl.h>
#include <CL/cl_platform.h>

int main(void)
{
    cl_int result;
    cl_uint opencl_platforms_amount = 0;
    cl_platform_id* platform_ids = NULL;

    result = clGetPlatformIDs(0, NULL, &opencl_platforms_amount);
    cl_ok_or_abort(result, "Error while getting OpenCL platforms!");

    platform_ids = malloc(opencl_platforms_amount * sizeof(cl_platform_id));
    if (platform_ids == NULL) {
        fputs("malloc", stderr);
    }

    result = clGetPlatformIDs(opencl_platforms_amount, platform_ids, NULL);
    cl_ok_or_abort(result, "Error while getting OpenCL platforms!");

    for (int platform = 0; platform < opencl_platforms_amount; platform++) {
        free(platform_ids[platform]);
    }
    free(platform_ids);
}
