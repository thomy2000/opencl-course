#include "opencl-setup.h"

#include <CL/cl.h>
#include <CL/cl_platform.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "bmpfuncs.h"

int main(int argc, char* argv[])
{
    opencl_t* opencl;
    bitmap_image_t image_input;
    bitmap_image_t image_output;
    cl_kernel kernel_image_scale;
    cl_kernel kernel_edge_detection;
    size_t kernel_image_scale_work_dimension[2];
    size_t kernel_edge_detection_work_dimension[2];
    cl_mem image_input_buffer;
    cl_mem image_processed_buffer;
    cl_mem image_output_buffer;

    // Set up context
    opencl = cl_create_context_cli();

    // Load the image data
    image_input = read_image("res/input.bmp");
    image_output.original_filename
        = malloc(strlen(image_input.original_filename) + 1);
    strcpy(image_output.original_filename, image_input.original_filename);
    image_output.width = image_input.width * 2;
    image_output.height = image_input.height * 2;
    image_output.data = malloc(image_size(image_input) * 4);
    kernel_image_scale_work_dimension[0] = image_input.width;
    kernel_image_scale_work_dimension[1] = image_input.height;
    kernel_edge_detection_work_dimension[0] = image_output.width;
    kernel_edge_detection_work_dimension[1] = image_output.height;

    // Create buffers and copy data
    image_input_buffer = cl_create_buffer(opencl,
        CL_MEM_READ_ONLY,
        image_size(image_input),
        NULL);
    image_processed_buffer = cl_create_buffer(opencl,
        CL_MEM_READ_WRITE,
        image_size(image_input) * 4,
        NULL);
    image_output_buffer = cl_create_buffer(opencl,
        CL_MEM_WRITE_ONLY,
        image_size(image_input) * 4,
        NULL);

    cl_enqueue_write_buffer(opencl,
        image_input_buffer,
        CL_TRUE,
        0,
        image_size(image_input),
        image_input.data,
        0,
        NULL,
        NULL);

    // Create kernels and set parameters
    kernel_image_scale = cl_create_kernel_with_source(opencl,
        "src/kernels/image_scale.cl",
        "image_scale_2x");
    cl_set_kernel_arg(kernel_image_scale,
        0,
        sizeof(size_t),
        &image_input_buffer);
    cl_set_kernel_arg(kernel_image_scale,
        1,
        sizeof(cl_int),
        &image_input.width);
    cl_set_kernel_arg(kernel_image_scale,
        2,
        sizeof(size_t),
        &image_processed_buffer);
    cl_set_kernel_arg(kernel_image_scale,
        3,
        sizeof(cl_int),
        &image_output.width);

    // Enqueue kernels (on same data, without copying back to host)
    cl_enqueue_nd_range_kernel(opencl,
        kernel_image_scale,
        2,
        NULL,
        kernel_image_scale_work_dimension,
        NULL,
        0,
        NULL,
        NULL);

    kernel_edge_detection = cl_create_kernel_with_source(opencl,
        "src/kernels/lab3_1.cl",
        "edge_detection_sobel");
    cl_set_kernel_arg(kernel_edge_detection,
        0,
        sizeof(size_t),
        &image_processed_buffer);
    cl_set_kernel_arg(kernel_edge_detection,
        1,
        sizeof(cl_int),
        &image_output.width);
    cl_set_kernel_arg(kernel_edge_detection,
        2,
        sizeof(size_t),
        &image_output_buffer);

    // Enqueue the kernel for execution and wait for it to finish
    cl_enqueue_nd_range_kernel(opencl,
        kernel_edge_detection,
        2,
        NULL,
        kernel_edge_detection_work_dimension,
        NULL,
        0,
        NULL,
        NULL);
    cl_finish(opencl);

    // Read the image from the buffers
    cl_enqueue_read_buffer(opencl,
        image_output_buffer,
        CL_TRUE,
        0,
        image_size(image_output),
        image_output.data,
        0,
        NULL,
        NULL);

    // Store the image to disk
    store_image(image_output, "/tmp/output.bmp");

    // Destroy everything
    cl_destroy_wrapper(opencl);
    cl_release_kernel(kernel_image_scale);
    cl_release_kernel(kernel_edge_detection);
    cl_release_memory_object(image_input_buffer);
    cl_release_memory_object(image_processed_buffer);
    cl_release_memory_object(image_output_buffer);
    destroy_image(image_input);
    destroy_image(image_output);
}
