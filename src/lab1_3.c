#include "opencl-setup.h"

#include <CL/cl.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char** argv)
{
    opencl_t* opencl;
    cl_kernel array_creation_kernel;
    cl_mem cl_buffer_input;
    cl_mem cl_buffer_output;
    char* buffer_output;
    size_t global_work_size;

    if (argc != 2) {
        printf("Please give the text to copy as the first argument.\n");
        return 1;
    }

    // Add one to the length to include string terminator.
    global_work_size = strlen(argv[1]) + 1;
    buffer_output = malloc(global_work_size * sizeof(char));

    opencl = cl_create_context_cli();

    cl_buffer_input = cl_create_buffer(opencl,
        CL_MEM_WRITE_ONLY,
        100 * sizeof(cl_int),
        NULL);
    cl_buffer_output = cl_create_buffer(opencl,
        CL_MEM_WRITE_ONLY,
        100 * sizeof(cl_int),
        NULL);

    // Create kernel and add arguments
    array_creation_kernel = cl_create_kernel_with_source(opencl,
        "src/kernels/lab1_3.cl",
        "copy_text");
    cl_set_kernel_arg(array_creation_kernel,
        0,
        sizeof(size_t),
        &cl_buffer_input);
    cl_set_kernel_arg(array_creation_kernel,
        1,
        sizeof(size_t),
        &cl_buffer_output);

    cl_enqueue_write_buffer(opencl,
        cl_buffer_input,
        CL_TRUE,
        0,
        global_work_size * sizeof(char),
        argv[1],
        0,
        NULL,
        NULL);

    // Enqueue the kernel for execution and wait for it to finish
    cl_enqueue_nd_range_kernel(opencl,
        array_creation_kernel,
        1,
        NULL,
        &global_work_size,
        NULL,
        0,
        NULL,
        NULL);
    cl_finish(opencl);

    // Kernel finished executing. Kernel stuff is no longer needed.
    cl_release_kernel(array_creation_kernel);
    cl_release_memory_object(cl_buffer_input);

    // Read the data from the buffer back to the host
    cl_enqueue_read_buffer(opencl,
        cl_buffer_output,
        CL_TRUE,
        0,
        global_work_size * sizeof(char),
        buffer_output,
        0,
        NULL,
        NULL);

    // Data has been read back to host memory. Release the corresponding buffer.
    cl_release_memory_object(cl_buffer_output);

    printf("%s\n", buffer_output);

    cl_destroy_wrapper(opencl);

    // Free the memory we are responsible for managing!
    free(buffer_output);

    return 0;
}
