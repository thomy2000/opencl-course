#include "opencl-setup.h"

#include <CL/cl.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct arguments {
    uint32_t matrix_equal_size;
    uint32_t matrix1_rows;
    uint32_t matrix2_cols;
} arguments;

/// Validate and parse program arguments.
arguments parse_arguments(int argc, char** argv)
{
    arguments result;

    if (argc != 4) {
        printf("Expected 3 arguments, got %d.\nWhen using MxN and NxO "
               "matrices, arguments are: M N O\n",
            argc - 1);
        exit(1);
    }

    result.matrix1_rows = atoi(argv[1]);
    result.matrix_equal_size = atoi(argv[2]);
    result.matrix2_cols = atoi(argv[3]);
    return result;
}

int main(int argc, char** argv)
{
    opencl_t* opencl;
    arguments program_arguments;
    cl_int* input_matrix1;
    cl_int* input_matrix2;
    cl_int* output_matrix;
    cl_mem input_matrix1_buffer;
    cl_mem input_matrix2_buffer;
    cl_mem output_matrix_buffer;
    cl_kernel matrix_multiply_kernel;
    size_t global_work_size[2];

    // Initialize the random number generator
    srand(time(NULL));

    // Validate and parse the command line arguments
    program_arguments = parse_arguments(argc, argv);

    global_work_size[0] = (size_t)program_arguments.matrix1_rows;
    global_work_size[1] = (size_t)program_arguments.matrix2_cols;

    // Allocate and fill the matrices
    input_matrix1 = malloc(sizeof(cl_int) * program_arguments.matrix1_rows
        * program_arguments.matrix_equal_size);
    input_matrix2 = malloc(sizeof(cl_int) * program_arguments.matrix2_cols
        * program_arguments.matrix_equal_size);
    output_matrix = malloc(sizeof(cl_int) * program_arguments.matrix1_rows
        * program_arguments.matrix2_cols);
    for (int index = 0; index
         < program_arguments.matrix_equal_size * program_arguments.matrix1_rows;
         index++) {
        input_matrix1[index] = (cl_int)rand() % 10;
    }
    for (int index = 0; index
         < program_arguments.matrix_equal_size * program_arguments.matrix2_cols;
         index++) {
        input_matrix2[index] = (cl_int)rand() % 10;
    }
    for (int index = 0; index
         < program_arguments.matrix1_rows * program_arguments.matrix2_cols;
         index++) {
        // Initialize with garbage data to make sure every location is written.
        output_matrix[index] = 7;
    }

    opencl = cl_create_context_cli();
    input_matrix1_buffer = cl_create_buffer(opencl,
        CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
        program_arguments.matrix1_rows * program_arguments.matrix_equal_size
            * sizeof(cl_int),
        (void*)input_matrix1);
    input_matrix2_buffer = cl_create_buffer(opencl,
        CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
        program_arguments.matrix2_cols * program_arguments.matrix_equal_size
            * sizeof(cl_int),
        (void*)input_matrix2);
    output_matrix_buffer = cl_create_buffer(opencl,
        CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
        program_arguments.matrix1_rows * program_arguments.matrix2_cols
            * sizeof(cl_int),
        (void*)output_matrix);

    // Create kernel and add arguments
    matrix_multiply_kernel = cl_create_kernel_with_source(opencl,
        "src/kernels/lab4_1.cl",
        "matrix_multiply");
    cl_set_kernel_arg(matrix_multiply_kernel,
        0,
        sizeof(size_t),
        (void*)&input_matrix1_buffer);
    cl_set_kernel_arg(matrix_multiply_kernel,
        1,
        sizeof(size_t),
        (void*)&input_matrix2_buffer);
    cl_set_kernel_arg(matrix_multiply_kernel,
        2,
        sizeof(size_t),
        (void*)&output_matrix_buffer);
    cl_set_kernel_arg(matrix_multiply_kernel,
        3,
        sizeof(cl_int),
        (void*)&program_arguments.matrix1_rows);
    cl_set_kernel_arg(matrix_multiply_kernel,
        4,
        sizeof(cl_int),
        (void*)&program_arguments.matrix_equal_size);
    cl_set_kernel_arg(matrix_multiply_kernel,
        5,
        sizeof(cl_int),
        (void*)&program_arguments.matrix2_cols);

    clock_t start = clock(), diff;
    // Enqueue the kernel for execution and wait for it to finish
    cl_enqueue_nd_range_kernel(opencl,
        matrix_multiply_kernel,
        2,
        NULL,
        global_work_size,
        NULL,
        0,
        NULL,
        NULL);
    cl_finish(opencl);
    diff = clock() - start;
    int msec = diff * 1000 / CLOCKS_PER_SEC;
    printf("Kernel took %d milliseconds\n", msec);

    cl_enqueue_read_buffer(opencl,
        output_matrix_buffer,
        CL_TRUE,
        0,
        program_arguments.matrix1_rows * program_arguments.matrix2_cols
            * sizeof(cl_int),
        (void*)output_matrix,
        0,
        NULL,
        NULL);

    // Free the memory we are responsible for managing!
    cl_destroy_wrapper(opencl);
    free(input_matrix1);
    free(input_matrix2);
    free(output_matrix);

    return 0;
}
