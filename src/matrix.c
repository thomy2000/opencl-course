#include <stdio.h>
#include <stdlib.h>

void fill_matrix_random(int rows, int cols, int matrix[rows][cols])
{
    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
            matrix[row][col] = rand() % 10;
        }
    }
}

void print_matrix(int rows, int cols, int matrix[rows][cols])
{
    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
            printf("%d ", matrix[row][col]);
        }
        printf("\n");
    }
}
