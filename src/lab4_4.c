#include "opencl-setup.h"

#include <CL/cl.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "matrix.h"
#include "random.h"

#define INPUT_ROWS 30
#define INPUT_COLS 50

int main(void)
{
    opencl_t* opencl;
    cl_mem cl_buffer_matrix_input, cl_buffer_matrix_output;
    cl_kernel kernel_matrix_transpose;
    int matrix_input[INPUT_ROWS][INPUT_COLS],
        matrix_output[INPUT_COLS][INPUT_ROWS];
    size_t global_work_size[2] = { INPUT_ROWS, INPUT_COLS };

    seed_random_number_generator();
    fill_matrix_random(INPUT_ROWS, INPUT_COLS, matrix_input);
    print_matrix(INPUT_ROWS, INPUT_COLS, matrix_input);

    opencl = cl_create_context_cli();
    cl_buffer_matrix_input = cl_create_buffer(opencl,
        CL_MEM_READ_ONLY,
        sizeof(matrix_input),
        NULL);
    cl_buffer_matrix_output = cl_create_buffer(opencl,
        CL_MEM_WRITE_ONLY,
        sizeof(matrix_output),
        NULL);
    cl_enqueue_write_buffer(opencl,
        cl_buffer_matrix_input,
        CL_TRUE,
        0,
        sizeof(matrix_input),
        matrix_input,
        0,
        NULL,
        NULL);
    kernel_matrix_transpose = cl_create_kernel_with_source(opencl,
        "src/kernels/matrix.cl",
        "matrix_transpose");

    cl_set_kernel_arg(kernel_matrix_transpose,
        0,
        sizeof(size_t),
        &cl_buffer_matrix_input);
    cl_set_kernel_arg(kernel_matrix_transpose,
        1,
        sizeof(cl_int),
        &global_work_size[0]);
    cl_set_kernel_arg(kernel_matrix_transpose,
        2,
        sizeof(cl_int),
        &global_work_size[1]);
    cl_set_kernel_arg(kernel_matrix_transpose,
        3,
        sizeof(size_t),
        &cl_buffer_matrix_output);
    cl_enqueue_nd_range_kernel(opencl,
        kernel_matrix_transpose,
        2,
        NULL,
        global_work_size,
        NULL,
        0,
        NULL,
        NULL);
    cl_finish(opencl);

    cl_enqueue_read_buffer(opencl,
        cl_buffer_matrix_output,
        CL_TRUE,
        0,
        sizeof(matrix_output),
        matrix_output,
        0,
        NULL,
        NULL);

    print_matrix(INPUT_COLS, INPUT_ROWS, matrix_output);

    cl_release_kernel(kernel_matrix_transpose);
    cl_release_memory_object(cl_buffer_matrix_output);
    cl_release_memory_object(cl_buffer_matrix_input);
    cl_destroy_wrapper(opencl);
}
