#include "opencl-setup.h"

#include <CL/cl.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(int argc, char** argv)
{
    int parameter_vector_size = 0;
    cl_int* vector1;
    cl_int* vector2;
    cl_int* result_vector;
    cl_mem vector1_buffer;
    cl_mem vector2_buffer;
    cl_mem result_vector_buffer;
    cl_kernel vector_multiplication_kernel;
    size_t global_work_size;
    opencl_t* opencl;

    // Initialize the random number generator
    srand(time(NULL));

    // Validate and parse the command line arguments
    if (argc != 2) {
        printf("expected 1 argument\n");
        return 1;
    }
    parameter_vector_size = atoi(argv[1]);
    global_work_size = parameter_vector_size;

    // Allocate and fill the vectors
    vector1 = malloc(sizeof(cl_int) * parameter_vector_size);
    vector2 = malloc(sizeof(cl_int) * parameter_vector_size);
    result_vector = malloc(sizeof(cl_int) * parameter_vector_size);
    for (int i = 0; i < parameter_vector_size; i++) {
        vector1[i] = (cl_int)rand() % 10;
        vector2[i] = (cl_int)rand() % 10;
    }

    opencl = cl_create_context_cli();

    vector1_buffer = cl_create_buffer(opencl,
        CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
        parameter_vector_size * sizeof(cl_int),
        (void*)vector1);
    vector2_buffer = cl_create_buffer(opencl,
        CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
        parameter_vector_size * sizeof(cl_int),
        (void*)vector2);
    result_vector_buffer = cl_create_buffer(opencl,
        CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
        parameter_vector_size * sizeof(cl_int),
        (void*)result_vector);

    // Create kernel and add arguments
    vector_multiplication_kernel = cl_create_kernel_with_source(opencl,
        "src/kernels/lab2_1.cl",
        "multiply_vectors");
    cl_set_kernel_arg(vector_multiplication_kernel,
        0,
        sizeof(size_t),
        (void*)&vector1_buffer);
    cl_set_kernel_arg(vector_multiplication_kernel,
        1,
        sizeof(size_t),
        (void*)&vector2_buffer);
    cl_set_kernel_arg(vector_multiplication_kernel,
        2,
        sizeof(size_t),
        (void*)&result_vector_buffer);

    // Enqueue the kernel for execution and wait for it to finish
    cl_enqueue_nd_range_kernel(opencl,
        vector_multiplication_kernel,
        1,
        NULL,
        &global_work_size,
        NULL,
        0,
        NULL,
        NULL);
    cl_finish(opencl);

    cl_enqueue_read_buffer(opencl,
        result_vector_buffer,
        CL_TRUE,
        0,
        parameter_vector_size * sizeof(cl_int),
        (void*)result_vector,
        0,
        NULL,
        NULL);

    for (int i = 0; i < parameter_vector_size; i++) {
        printf("%d\t", result_vector[i]);
    }
    printf("\n");

    // Free the memory we are responsible for managing!
    cl_destroy_wrapper(opencl);
    free(vector1);
    free(vector2);
    free(result_vector);

    return 0;
}
