#pragma once

#ifndef CL_TARGET_OPENCL_VERSION
#define CL_TARGET_OPENCL_VERSION 200
#endif

#include <CL/cl.h>
#include <CL/cl_platform.h>
#include <stddef.h>

/*
 * Struct with the opencl context, required to use all the other functions.
 */
typedef struct opencl opencl_t;

/*
 * Get all the available platform IDs.
 * Abort the program on errors.
 *
 * The caller is responsible for freeing the returned memory.
 */
cl_uint cl_get_platform_ids(cl_platform_id** platform_ids);

/*
 * Given a list of opencl_platform_ids, let the user choose one of them through
 * the command line.
 */
cl_platform_id cl_choose_platform(cl_platform_id* opencl_platform_ids,
    cl_uint opencl_platforms_amount);

/*
 * Get all the available devices for the given platform.
 * Abort the program on errors.
 *
 * The caller is responsible for freeing the returned memory.
 */
cl_uint cl_get_device_ids(cl_platform_id platform_id,
    cl_device_id** device_ids);

/*
 * Create an OpenCL context.
 * Ask the user through stdin for which platform.
 * Abort the program on errors.
 *
 * The caller is responsible for releasing the context with `clReleaseContext()`
 * or `cl_release_context()`.
 */
cl_context cl_create_context(cl_platform_id* opencl_platform_ids,
    cl_uint opencl_platforms_amount,
    cl_device_id* opencl_device_ids,
    cl_uint opencl_devices_amount);

/*
 * Free the OpenCL command queue, aborting the process upon error.
 */
void cl_release_command_queue(cl_command_queue opencl_command_queue);

cl_kernel cl_create_kernel_with_source(
    opencl_t* opencl, char* source_file_path, const char* kernel_name);

/*
 * Finish all the remaining commands in the command queue.
 */
void cl_finish(opencl_t* opencl);

void cl_set_kernel_arg(cl_kernel kernel,
    cl_uint arg_index,
    size_t arg_size,
    const void* arg_value);

/*
 * Check whether `result` is `CL_SUCCESS`, otherwise exit with `error_message`.
 */
void cl_ok_or_abort(cl_int result, char* error_message);

/*
 * Create an OpenCL command queue with the given properties.
 */
cl_command_queue cl_create_command_queue_with_properties(
    cl_context opencl_context,
    cl_device_id opencl_device,
    const cl_queue_properties* opencl_queue_properties);

/*
 * Create an OpenCL buffer.
 */
cl_mem cl_create_buffer(
    opencl_t* opencl, cl_mem_flags memory_flags, size_t size, void* host_ptr);

/*
 * Enqueue an OpenCL kernel for execution.
 */
void cl_enqueue_nd_range_kernel(opencl_t* opencl,
    cl_kernel opencl_kernel,
    cl_uint work_dimension,
    const size_t* global_work_offset,
    const size_t* global_work_size,
    const size_t* local_work_size,
    cl_uint number_events_in_waitlist,
    const cl_event* event_wait_list,
    cl_event* event);

/*
 * Helper function to create a cl_context by asking questions on the command
 * line.
 */
opencl_t* cl_create_context_cli();

/*
 * Deallocate `opencl`.
 */
void cl_destroy_wrapper(opencl_t* opencl);

/**
 * Deallocate `kernel`.
 */
void cl_release_kernel(cl_kernel kernel);

/**
 * Deallocate `memory_object`.
 */
void cl_release_memory_object(cl_mem memory_object);

/*
 * Enqueue data copy from `ptr` into `buffer`.
 */
void cl_enqueue_write_buffer(opencl_t* opencl,
    cl_mem buffer,
    cl_bool blocking_write,
    size_t offset,
    size_t size,
    const void* ptr,
    cl_uint num_events_in_waitlist,
    const cl_event* event_wait_list,
    cl_event* event);

/*
 * Enqueue data copy from `buffer` into `ptr`.
 */
void cl_enqueue_read_buffer(opencl_t* opencl,
    cl_mem buffer,
    cl_bool blocking_read,
    size_t offset,
    size_t size,
    void* ptr,
    cl_uint num_events_in_waitlist,
    const cl_event* event_wait_list,
    cl_event* event);
