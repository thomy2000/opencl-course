#include "opencl-setup.h"

#include <CL/cl.h>
#include <stddef.h>
#include <stdio.h>

int main(void)
{
    opencl_t* opencl;
    cl_mem opencl_array_buffer;
    cl_kernel array_creation_kernel;
    cl_int number_array[100];
    size_t global_work_size = 100;

    opencl = cl_create_context_cli();

    // Create command queue and buffers
    opencl_array_buffer = cl_create_buffer(opencl,
        CL_MEM_WRITE_ONLY,
        100 * sizeof(cl_int),
        NULL);

    // Create kernel and add arguments
    array_creation_kernel = cl_create_kernel_with_source(opencl,
        "src/kernels/lab1_1.cl",
        "build_array");
    cl_set_kernel_arg(array_creation_kernel,
        0,
        sizeof(size_t),
        (void*)&opencl_array_buffer);

    // Enqueue the kernel for execution and wait for it to finish
    cl_enqueue_nd_range_kernel(opencl,
        array_creation_kernel,
        1,
        NULL,
        &global_work_size,
        NULL,
        0,
        NULL,
        NULL);
    cl_finish(opencl);

    // Kernel finished executing. Release it.
    cl_release_kernel(array_creation_kernel);

    // Read the data from the buffer back to the host
    cl_enqueue_read_buffer(opencl,
        opencl_array_buffer,
        CL_TRUE,
        0,
        100 * sizeof(cl_int),
        (void*)number_array,
        0,
        NULL,
        NULL);

    // Data read back to the host. Release the OpenCL buffer.
    cl_release_memory_object(opencl_array_buffer);

    for (int i = 0; i < 100; i++) {
        printf("%d ", number_array[i]);
    }
    printf("\n");

    cl_destroy_wrapper(opencl);

    return 0;
}
