#include "opencl-setup.h"

#include <CL/cl.h>
#include <CL/cl_platform.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * Free the OpenCL context, aborting the process upon error.
 */
void cl_release_context(cl_context opencl_context);

struct opencl {
    cl_context context;
    cl_uint devices_amount;
    cl_device_id* devices;
    cl_command_queue command_queue;
};

cl_uint cl_get_platform_ids(cl_platform_id** platform_ids)
{
    cl_int result;
    cl_uint opencl_platforms_amount = 0;

    result = clGetPlatformIDs(0, NULL, &opencl_platforms_amount);
    cl_ok_or_abort(result, "Error while getting OpenCL platforms!");

    *platform_ids = malloc(opencl_platforms_amount * sizeof(cl_platform_id));
    if (platform_ids == NULL) {
        fputs("malloc", stderr);
    }

    result = clGetPlatformIDs(opencl_platforms_amount, *platform_ids, NULL);
    cl_ok_or_abort(result, "Error while getting OpenCL platforms!");

    return opencl_platforms_amount;
}

cl_platform_id cl_choose_platform(cl_platform_id* opencl_platform_ids,
    cl_uint opencl_platforms_amount)
{
    cl_int result;

    printf("Platforms:\n");

    for (uint32_t i = 0; i < opencl_platforms_amount; i++) {
        size_t name_size = 0;

        result = clGetPlatformInfo(opencl_platform_ids[i],
            CL_PLATFORM_NAME,
            0,
            NULL,
            &name_size);
        cl_ok_or_abort(result, "Error getting platform name size!");

        char* name = malloc(name_size * sizeof(char));

        result = clGetPlatformInfo(opencl_platform_ids[i],
            CL_PLATFORM_NAME,
            name_size,
            name,
            &name_size);
        cl_ok_or_abort(result, "Error getting platform name!");

        printf("%d. %s\n", i, name);

        free(name);
    }

    uint32_t opencl_platform_choice = 0;

    do {
        printf("Choose an OpenCL platform to use: ");
        scanf("%d%*c", &opencl_platform_choice);
    } while (opencl_platform_choice >= opencl_platforms_amount);

    return opencl_platform_ids[opencl_platform_choice];
}

cl_uint cl_get_device_ids(cl_platform_id platform_id, cl_device_id** device_ids)
{
    cl_int result;
    cl_uint opencl_devices_amount = 0;

    result = clGetDeviceIDs(platform_id,
        CL_DEVICE_TYPE_ALL,
        0,
        NULL,
        &opencl_devices_amount);
    cl_ok_or_abort(result, "Error while getting OpenCL devices!");

    *device_ids = malloc(opencl_devices_amount * sizeof(cl_device_id));

    result = clGetDeviceIDs(platform_id,
        CL_DEVICE_TYPE_ALL,
        opencl_devices_amount,
        *device_ids,
        NULL);
    cl_ok_or_abort(result, "Error while getting OpenCL devices!");

    return opencl_devices_amount;
}

cl_context cl_create_context(cl_platform_id* opencl_platform_ids,
    cl_uint opencl_platforms_amount,
    cl_device_id* opencl_device_ids,
    cl_uint opencl_devices_amount)
{
    cl_int result;

    printf("Devices:\n");
    for (uint32_t i = 0; i < opencl_devices_amount; i++) {
        size_t name_size = 0;

        result = clGetDeviceInfo(opencl_device_ids[i],
            CL_DEVICE_NAME,
            0,
            NULL,
            &name_size);
        cl_ok_or_abort(result, "Error getting device name size!");

        char* name = malloc(name_size * sizeof(char));

        result = clGetDeviceInfo(opencl_device_ids[i],
            CL_DEVICE_NAME,
            name_size,
            name,
            &name_size);
        cl_ok_or_abort(result, "Error getting device name!");

        printf("%d. %s\n", i, name);

        free(name);
    }

    cl_context opencl_context = clCreateContext(NULL,
        opencl_devices_amount,
        opencl_device_ids,
        NULL,
        NULL,
        &result);
    cl_ok_or_abort(result, "Error creating OpenCL context!");

    return opencl_context;
}

void cl_release_context(cl_context opencl_context)
{
    cl_int result;

    // Release the context, lowering its reference count.
    result = clReleaseContext(opencl_context);
    cl_ok_or_abort(result, "Failed to free OpenCL context!");
}

void cl_release_command_queue(cl_command_queue opencl_command_queue)
{
    cl_int result;

    result = clReleaseCommandQueue(opencl_command_queue);
    cl_ok_or_abort(result, "Failed to free OpenCL command queue!");
}

cl_kernel cl_create_kernel_with_source(
    opencl_t* opencl, char* source_file_path, const char* kernel_name)
{
    cl_int result;
    cl_kernel result_kernel;
    cl_program source_program;
    FILE* fp;
    long l_size;
    char* buffer;

    fp = fopen(source_file_path, "rb");
    if (!fp)
        perror(source_file_path), exit(1);

    fseek(fp, 0L, SEEK_END);
    l_size = ftell(fp);
    rewind(fp);

    /* allocate memory for entire content */
    buffer = calloc(1, l_size + 1);
    if (!buffer)
        fclose(fp), fputs("memory alloc fails", stderr), exit(1);

    /* copy the file into the buffer */
    if (1 != fread(buffer, l_size, 1, fp))
        fclose(fp), free(buffer), fputs("entire read fails", stderr), exit(1);

    fclose(fp);

    // NOTE: I'm not sure whether it's correct to cast to a `const char**` here
    source_program = clCreateProgramWithSource(opencl->context,
        1,
        (const char**)&buffer,
        NULL,
        NULL);

    result = clBuildProgram(source_program, 0, NULL, NULL, NULL, NULL);
    cl_ok_or_abort(result, "Failed to build the program!");

    result_kernel = clCreateKernel(source_program, kernel_name, &result);
    cl_ok_or_abort(result, "Couldn't create kernel!");

    free(buffer);

    return result_kernel;
}

void cl_finish(opencl_t* opencl)
{
    cl_ok_or_abort(clFinish(opencl->command_queue),
        "Couldn't finish command queue.");
}

void cl_set_kernel_arg(
    cl_kernel kernel, cl_uint arg_index, size_t arg_size, const void* arg_value)
{
    cl_int result = clSetKernelArg(kernel, arg_index, arg_size, arg_value);
    cl_ok_or_abort(result, "Couldn't set kernel argument!");
}

void cl_ok_or_abort(cl_int result, char* error_message)
{
    if (result != CL_SUCCESS) {
        fputs(error_message, stderr);
        fputc('\n', stderr);

        exit(1);
    }
}

cl_command_queue cl_create_command_queue_with_properties(
    cl_context opencl_context,
    cl_device_id opencl_device,
    const cl_queue_properties* opencl_queue_properties)
{
    cl_int result;
    cl_command_queue command_queue = NULL;

    command_queue = clCreateCommandQueueWithProperties(opencl_context,
        opencl_device,
        NULL,
        &result);
    cl_ok_or_abort(result, "Couldn't creaete command queue with properties!");

    return command_queue;
}

cl_mem cl_create_buffer(
    opencl_t* opencl, cl_mem_flags memory_flags, size_t size, void* host_ptr)
{
    cl_int result = 0;
    cl_mem buffer = NULL;

    buffer = clCreateBuffer(opencl->context,
        memory_flags,
        size,
        host_ptr,
        &result);
    cl_ok_or_abort(result, "Couldn't create buffer!");

    return buffer;
}

void cl_enqueue_nd_range_kernel(opencl_t* opencl,
    cl_kernel opencl_kernel,
    cl_uint work_dimension,
    const size_t* global_work_offset,
    const size_t* global_work_size,
    const size_t* local_work_size,
    cl_uint number_events_in_waitlist,
    const cl_event* event_wait_list,
    cl_event* event)
{
    cl_int result = 0;

    clEnqueueNDRangeKernel(opencl->command_queue,
        opencl_kernel,
        work_dimension,
        global_work_offset,
        global_work_size,
        local_work_size,
        number_events_in_waitlist,
        event_wait_list,
        event);
    cl_ok_or_abort(result, "Couldn't execute kernel!");
}

opencl_t* cl_create_context_cli()
{
    opencl_t* opencl = malloc(sizeof(opencl_t));
    cl_uint opencl_platforms_amount = 0;
    cl_platform_id* opencl_platform_ids = NULL;
    cl_platform_id opencl_chosen_platform = NULL;

    opencl_platforms_amount = cl_get_platform_ids(&opencl_platform_ids);
    opencl_chosen_platform
        = cl_choose_platform(opencl_platform_ids, opencl_platforms_amount);
    opencl->devices_amount
        = cl_get_device_ids(opencl_chosen_platform, &opencl->devices);
    opencl->context = cl_create_context(opencl_platform_ids,
        opencl_platforms_amount,
        opencl->devices,
        opencl->devices_amount);
    opencl->command_queue
        = cl_create_command_queue_with_properties(opencl->context,
            opencl->devices[0],
            NULL);

    free(opencl_platform_ids);
    return opencl;
}

void cl_destroy_wrapper(opencl_t* opencl)
{
    cl_release_command_queue(opencl->command_queue);
    cl_release_context(opencl->context);
    for (int device = 0; device < opencl->devices_amount; device++) {
        clReleaseDevice(opencl->devices[device]);
    }
    free(opencl->devices);
    free(opencl);
}

void cl_release_kernel(cl_kernel kernel)
{
    cl_int result = 0;

    result = clReleaseKernel(kernel);
    cl_ok_or_abort(result, "Couldn't release kernel!");
}

void cl_release_memory_object(cl_mem memory_object)
{
    cl_int result = 0;

    result = clReleaseMemObject(memory_object);
    cl_ok_or_abort(result, "Couldn't release memory object!");
}

void cl_enqueue_write_buffer(opencl_t* opencl,
    cl_mem buffer,
    cl_bool blocking_write,
    size_t offset,
    size_t size,
    const void* ptr,
    cl_uint num_events_in_waitlist,
    const cl_event* event_wait_list,
    cl_event* event)
{
    cl_ok_or_abort(clEnqueueWriteBuffer(opencl->command_queue,
                       buffer,
                       blocking_write,
                       offset,
                       size,
                       ptr,
                       num_events_in_waitlist,
                       event_wait_list,
                       event),
        "Couldn't write data to buffer!");
}

void cl_enqueue_read_buffer(opencl_t* opencl,
    cl_mem buffer,
    cl_bool blocking_read,
    size_t offset,
    size_t size,
    void* ptr,
    cl_uint num_events_in_waitlist,
    const cl_event* event_wait_list,
    cl_event* event)
{
    cl_ok_or_abort(clEnqueueReadBuffer(opencl->command_queue,
                       buffer,
                       blocking_read,
                       offset,
                       size,
                       ptr,
                       num_events_in_waitlist,
                       event_wait_list,
                       event),
        "Couldn't read data from buffer!");
}
