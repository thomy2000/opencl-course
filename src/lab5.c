#include "opencl-setup.h"
#include "random.h"

#include <CL/cl.h>
#include <bits/types/struct_timeval.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

#define BODIES_AMOUNT 30000
#define SIMULATION_ITERATIONS 10

/**
 * A body in a 3D space.
 */
typedef struct body {
    // Position
    float x, y, z;
    // Velocity
    float vx, vy, vz;
} body_t;

typedef struct program_arguments {
    int32_t bodies_amount;
    int32_t simulation_iterations;
} program_arguments_t;

program_arguments_t parse_program_arguments(int argc, char** argv)
{
    program_arguments_t program_arguments = {
        .bodies_amount = BODIES_AMOUNT,
        .simulation_iterations = SIMULATION_ITERATIONS,
    };

    if (argc > 1) {
        if (strcmp("-h", argv[1]) == 0 || strcmp("--help", argv[1]) == 0) {
            printf("[bodies] [iterations]\n");
            exit(0);
        }
        program_arguments.bodies_amount = atoi(argv[1]);
    }

    if (argc > 2) {
        program_arguments.simulation_iterations = atoi(argv[2]);
    }

    return program_arguments;
}

void randomize_bodies(body_t* bodies, int32_t bodies_amount)
{
    for (int32_t body = 0; body < bodies_amount; body++) {
        float* current_body = (float*)&bodies[body];

        for (int32_t part = 0; part < 6; part++) {
            current_body[part] = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
        }
    }
}

void print_bodies(body_t* bodies, int32_t bodies_amount)
{
    for (int32_t body = 0; body < bodies_amount; body++) {
        body_t current_body = bodies[body];
        printf("Body %d: x = %.3f, y = %.3f, z = %.3f; vx = %.3f, vy = %.3f, "
               "vz = %.3f\n",
            body,
            current_body.x,
            current_body.y,
            current_body.z,
            current_body.vx,
            current_body.vy,
            current_body.vz);
    }
}

void simulate_bodies(body_t* bodies, int32_t bodies_amount, int32_t iterations)
{
    opencl_t* opencl = NULL;
    cl_kernel kernel_nbodies = NULL;
    cl_mem buffer_bodies = NULL;
    struct timeval time_start, time_stop, time_elapsed;
    double total_time = 0.0;
    size_t global_work_size = (size_t)bodies_amount;
    print_bodies(bodies, bodies_amount);

    opencl = cl_create_context_cli();
    kernel_nbodies = cl_create_kernel_with_source(opencl,
        "src/kernels/nbodies.cl",
        "nbodies");
    buffer_bodies = cl_create_buffer(opencl,
        CL_MEM_READ_WRITE,
        bodies_amount * sizeof(body_t),
        NULL);

    cl_enqueue_write_buffer(opencl,
        buffer_bodies,
        CL_TRUE,
        0,
        bodies_amount * sizeof(body_t),
        bodies,
        0,
        NULL,
        NULL);
    cl_set_kernel_arg(kernel_nbodies, 0, sizeof(size_t), &buffer_bodies);
    cl_set_kernel_arg(kernel_nbodies, 1, sizeof(cl_int), &bodies_amount);

    for (int32_t iteration = 1; iteration <= iterations; iteration++) {
        double iteration_time_elapsed = 0.0;
        gettimeofday(&time_start, NULL);

        cl_enqueue_nd_range_kernel(opencl,
            kernel_nbodies,
            1,
            0,
            &global_work_size,
            NULL,
            0,
            NULL,
            NULL);
        cl_finish(opencl);

        gettimeofday(&time_stop, NULL);
        timersub(&time_stop, &time_start, &time_elapsed);
        iteration_time_elapsed
            = (time_elapsed.tv_sec * 1000.0 + time_elapsed.tv_usec / 1000.0)
            / 1000;
        if (iteration > 1) {
            total_time += iteration_time_elapsed;
        }
        printf("Iteration %d: %.3f seconds\n",
            iteration,
            iteration_time_elapsed);
    }

    cl_enqueue_read_buffer(opencl,
        buffer_bodies,
        CL_TRUE,
        0,
        bodies_amount * sizeof(body_t),
        bodies,
        0,
        NULL,
        NULL);

    print_bodies(bodies, bodies_amount);
    printf("Total runtime: %.3f seconds\n", total_time);

    cl_release_memory_object(buffer_bodies);
    cl_release_kernel(kernel_nbodies);
    cl_destroy_wrapper(opencl);
}

int main(int argc, char** argv)
{
    program_arguments_t program_arguments = parse_program_arguments(argc, argv);
    body_t* bodies = malloc(program_arguments.bodies_amount * sizeof(body_t));
    seed_random_number_generator();

    if (bodies == NULL) {
        fputs("malloc failed\n", stderr);
        exit(1);
    }

    randomize_bodies(bodies, program_arguments.bodies_amount);
    simulate_bodies(bodies,
        program_arguments.bodies_amount,
        program_arguments.simulation_iterations);

    free(bodies);
    return 0;
}
