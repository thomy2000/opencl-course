#pragma once

/*
 * Seed the random number generator with a random seed.
 */
void seed_random_number_generator(void);
