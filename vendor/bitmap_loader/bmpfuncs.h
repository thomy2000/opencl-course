#ifndef __BMPFUNCS__
#define __BMPFUNCS__

#include <stdint.h>
typedef unsigned char uchar;

typedef struct bitmap_image {
    char* original_filename;
    float* data;
    int width;
    int height;
} bitmap_image_t;

float* readImage(const char* filename, int* widthOut, int* heightOut);
void storeImage(float* imageOut,
    const char* filename,
    int rows,
    int cols,
    const char* refFilename);

/*
 * Load an image from `filename`.
 */
bitmap_image_t read_image(const char* filename);

/*
 * Save `image` to the path given in `filename`.
 */
void store_image(bitmap_image_t image, const char* filename);

/*
 * Deallocate the resources assigned to `image`.
 */
void destroy_image(bitmap_image_t image);

/*
 * Return the size of the image in bytes.
 */
uintptr_t image_size(bitmap_image_t image);

#endif
